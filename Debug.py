#!/usr/bin/env python
# -*- coding: utf-8 -*-

import inspect
from pprint import *

def debug(*params):

  DEBUGGING_LEVEL = 1

  if DEBUGGING_LEVEL == 1:
    log = ''
    for param in params:
      log += str(param) + ', '

    cname = inspect.stack()[1][3]
    cclass = get_class_from_frame(inspect.stack()[1][0])

    if len(params) != 0:
      log = ': ' + log

    #~ try:
      #~ if cclass in ['Server', 'Client', 'NewGameController', 'Player', 'PlayersList'] or params[0] == 'PL':
    print '> {0} - {1}{2}'.format(cclass, cname, log[:-2])
    #~ except IndexError:
      #~ pass
    #~ print '> {0}{1}'.format(cname, log[:-2])

def get_class_from_frame(fr):
  # http://stackoverflow.com/questions/2203424/python-how-to-retrieve-class-information-from-a-frame-object
  args, _, _, value_dict = inspect.getargvalues(fr)
  # we check the first parameter for the frame function is
  # named 'self'
  if len(args) and args[0] == 'self':
    # in that case, 'self' will be referenced in value_dict
    instance = value_dict.get('self', None)
    if instance:
      # return its class
      #~ return getattr(instance, '__class__', None)
      return instance.__class__.__name__
  # return None otherwise
  return None

if __name__ == '__main__':
  import Run
  Run.main()

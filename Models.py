#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports
from socket import *
from Debug  import *
import ConfigParser
import threading
import inspect
import thread
import errno
import json
import re

class Server(object):
  def __init__(self):
    self.__playersList = None
    self.__callback = None
  def setCallback(self, callback):
    self.__callback = callback
  def start(self, port, playersList): # http://www.pvmgarage.com/2011/03/a-multi-threaded-persistent-echo-socket-server-in-python/
    if self.__callback is None:
      debug('missing callback')
      return False
    self.__playersList = playersList
    self.__cc = CC(self.__send)
    self.send = self.__cc.send
    #~ host = gethostname()
    #~ port = 0
    host = '127.0.0.1'
    self.__bufsize = 1024
    uri = (str(host), int(port))
    self.__socket = socket(AF_INET, SOCK_STREAM)
    self.__socket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    try:
      self.__socket.bind(uri)
      self.__socket.listen(4) # we want max. 4 connection
    except error, e:
      self.__callback.connectError(e)
      return False
    else:
      thread.start_new_thread(self.__server, ())
      debug(port, gethostname(), self.__socket.getsockname())
      return True
  def __server(self):
    """ Class for accepting client connections """
    self.__activeAccept = True
    while self.__activeAccept and len(self.__playersList) < 4:
      try:
        clientstock = self.__socket.accept()
      except error:
        debug('ACCEPT FAILED, server going down!')
        break
      else:
        playerID = self.__callback.newClient(clientstock)
        thread.start_new_thread(self.__client, (playerID,))
    self.__activeAccept = False
    debug('server accept thread shutdown')
  def __client(self, playerID):
    """ Class for interaction with clients """
    while True:
      try:
        data = self.__playersList[playerID].socket.recv(self.__bufsize)
        if not data:
          debug('connection closed [DATA] from ' + str(str(playerID)))
          if playerID in self.__playersList.keys():
            self.stop(playerID)
          break
      except error:
        debug('connection closed [RECV] from ' + str(playerID))
        break
      else:
        debug(self.__playersList[playerID], data)
        data = self.__cc.parse(data)
        self.__callback.serverReceive(playerID, data)
    debug('END OF SERVER-CLIENT THREAD')
  def stop(self, playerID=None):
    if playerID:
      debug(playerID)
      socket = self.__playersList[playerID].socket
      self.__callback.playerDisconected(playerID)
    else:
      debug('SERVER', self.__playersList)
      socket = self.__socket
      players = self.__playersList.keys()
      for playerID in players:
        #~ del self.__playersList[playerID]
        self.stop(playerID)
    try:
      socket.shutdown(SHUT_RDWR)
      socket.close()
    except error:
      debug('endpoint not connected')
      pass
  def __send(self, playerID, data):
    debug(playerID, data)
    debug(self.__playersList)
    data = json.dumps(data)
    if playerID == self.__cc.ALL:
      for player in self.__playersList.values():
        try:
          player.socket.send(data)
        except error, e:
          debug('first', e)
          #~ self.__callback.socketError(e)
    else:
      try:
        self.__playersList[playerID].socket.send(data)
      except error, e:
        debug('second', e)
        #~ self.__callback.socketError(e)
  def deactiveAccept(self):
    debug()
    self.__activeAccept = False
  def info(self):
    return self.__socket.getsockname() # << proč jsem to kurva napsal??

class Client(object):
  def __init__(self):
    self.__cc = CC(self.__send)
    self.send = self.__cc.send
    self.__running = False
  def setCallback(self, callback):
    self.__callback = callback
  def start(self, host, port):
    debug(host, port)
    self.__bufsize = 1024
    self.__uri = (str(host), int(port))
    self.__socket = socket(AF_INET, SOCK_STREAM)
    try:
      self.__socket.connect(self.__uri)
    except error, e:
      errorcode = e[0]
      if errorcode == errno.ECONNREFUSED:
        debug('CONNECTION REFUSED!')
        self.__callback.refused()
      else:
        debug('SOCKET ERROR:', e)
    else:
      self.__running = True
      thread.start_new_thread(self.__listen, ())
  def __listen(self):
    self.__callback.connected()
    debug(self.__running)
    while True:
      try:
        data = self.__socket.recv(self.__bufsize)
      except error, e:
        debug('error', e)
        if self.__running:
          self.__callback.clientStopped()
        self.stop()
        break
      if not data:
        debug('NOT DATA')
        if self.__running:
          self.__callback.clientStopped()
        self.stop()
        break
      #~ debug(data)
      multipleData = data.split('}{')
      if len(multipleData) != 1:
        #~ debug('**************************************************************', '#', multipleData, '#')
        data = multipleData[0] + '}'
        #~ debug('**************************************************************', data)
        data = self.__cc.parse(data)
        self.__callback.clientReceive(data)

        for data in multipleData[1:-1]:
          #~ debug('**************************************************************', '{' + data + '}')
          data = self.__cc.parse('{' + data + '}')
          self.__callback.clientReceive(data)

        data = '{' + multipleData[-1]
        #~ debug('**************************************************************', data)
        data = self.__cc.parse(data)
        self.__callback.clientReceive(data)
      else:
        data = self.__cc.parse(multipleData[0])
        self.__callback.clientReceive(data)
    debug('END OF CLIENT THREAD')
  def __send(self, data):
    # Convert data to JSON compatible string
    data = json.dumps(data)
    # Then send it to the client
    self.__socket.send(data)
  def stop(self):
    debug('socket SHUTDOWN')
    if self.__running:
      self.__running = False
      try:
        self.__socket.shutdown(SHUT_RDWR)
        self.__socket.close()
      except error:
        debug('endpoint not connected')
        pass
  def serverInfo(self):
    return self.__uri

class Player(object):
  def __init__(self):
    debug('NEW PLAYER')
  nick = 'Player'
  uri = ()
  socket = None
  def __repr__(self):
    return self.nick
  def __del__(self):
    debug('PLAYER ' + self.nick + ' DELETED!', 'socket:' + str(self.socket) + ' | uri:' + str(self.uri))

class CC(object):
  """ Communication Templates Class :) """
  # Constants
  ALL = 'ALL'
  COMMAND = 'CC'
  NICK = 'RNI'
  ALLPLAYERS = 'RAP'
  STARTGAME = 'RSG'
  PLAY = 'RPY'
  FRESHCARD = 'RFC'
  ACE = 'RAE'
  OUTOFCARDS = 'ROC'
  NEWNICK = 'ANN'
  STATE = 'ASE'
  ALLCARDS = 'AAC'
  CURRENTCARD = 'ACC'
  CURRENTPLAYER = 'ACP'
  NEWCARD = 'ANC'
  PLAYED = 'APY'
  WIN = 'AWN'

  def __init__(self, send):
    self.__send = send
    args = inspect.getargspec(send)
    if 'playerID' in args[0]:
      self.send = self.__SERVER_SEND
    else:
      self.send = self.__CLIENT_SEND

  def __SERVER_SEND(self, playerID, command, *data):
    debug(playerID, command)
    if data:
      self.__send(playerID, {CC.COMMAND:command, 'data':data[0]})
    else:
      self.__send(playerID, {CC.COMMAND:command})

  def __CLIENT_SEND(self, command, *data):
    debug(command)
    if data:
      self.__send({CC.COMMAND:command, 'data':data[0]})
    else:
      self.__send({CC.COMMAND:command})

  def parse(self, data):
    try:
      data = json.loads(data)
    except ValueError:
      debug('received data aren\'t valid json dump')
      debug('#'+data+'#')
    else:

      command = data[self.COMMAND]
      if 'data' in data:
        data = data['data']
      else:
        data = None
      return (command, data)

class Settings(object):

  __settingsFile = 'player.cfg'

  def __init__(self):
    self.__config = ConfigParser.RawConfigParser()
    self.__config.read(self.__settingsFile)

  def get(self, section, key):
    if self.__config.has_section(section):
      return self.__config.get(section, key)
    else:
      return None

  def set(self, section, key, value):
    if not self.__config.has_section(section):
      self.__config.add_section(section)
    self.__config.set(section, key, value)
    with open(self.__settingsFile, 'wb') as configfile:
      self.__config.write(configfile)

class Rules(object):
  Cards = ['7', '8', '9', '10', 'J', 'K', 'A', 'S'] # J - jack (spodek), K - king, A - ace (eso), S - Sportsman (svršek :P)
  Suits = ['H', 'S', 'A', 'L'] # Heart, Sphere, Acorn, Leaf

  class Check(object):
    def normal(self, currentCard, newCard):
      cards = self.__split(currentCard, newCard)
      if cards[0][0] == cards[1][0] or cards[0][1] == cards[1][1]:
        debug('normal', True, cards)
        return True
      elif cards[1][1] == 'S':
        debug('normal SPORTSMAN OVERRIDE', True, cards)
        return True
      else:
        debug('normal', False, cards)
        return False
    def onlyValue(self, currentCard, newCard):
      cards = self.__split(currentCard, newCard)
      if cards[0][1] == cards[1][1]:
        debug('onlyValue', True, cards)
        return True
      else:
        debug('onlyValue', False, cards)
        return False
    def __split(self, currentCard, newCard):
      currentSuit = currentCard[0]
      currentValue = currentCard[1:]
      newSuit = newCard[0]
      newValue = newCard[1:]
      return [[currentSuit, currentValue], [newSuit, newValue]]

if __name__ == '__main__':
  import Run
  Run.main()

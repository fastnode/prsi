#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports
from Models import CC
from Debug  import *
#~ from Rules  import *
import random
import Models
import Views
import sys
import re
import os

class Controller(object):
  """ Template for partial controllers classes """

  # Protected variable view for view object handle
  _view = None

  def __init__(self, v, o, m):
    self._view = v
    self._objects = o
    self._main = m
    self.name = self.__class__.__name__[:-10]

  def __repr__(self): # http://stackoverflow.com/questions/1436703/difference-between-str-and-repr-in-python
    return self.name

  def setView(self, view, **kwargs):
    self._view.setView(view, self, **kwargs)

  def run(self):
    self.setView(self.name)

  # Views shared callbacks
  def backToMenu(self):
    debug()
    self._main.setController('Menu')

class ClientExtension(Controller):
  """ Extends classes works with Client object with necessary methods """

  def clientReceive(self, data):
    com, data = data

    if com == CC.NICK: # Return players nick from local config file
      self.__nick = self._objects['settings'].get('Player', 'nick')
      self._objects['client'].send(CC.NICK, {'nick':self.__nick})

    elif com == CC.STARTGAME: # React to STARTGAME request by setting new controller
      self._main.setController('Game', nick=self.__nick)

    elif com == CC.NEWNICK: # Warn user that there is another player with same nick on server
      self.__nick = str(data['nick'])
        #~ self._view.showWarning('Upozornění', 'Na serveru se nachází hráč se stejným nickem. Budeš viditelný jako ' + str(data['nick']))

  # Methods required by Client (must be included even they do nothing)
  def refused(self):
    debug()
  def clientStopped(self):
    debug()
  def connected(self):
    debug()

class MainController(object):
  """ Main controller which create partial controllers and provide access to global objects """

  __objects = {'server' : None, 'client' : None, 'playerslist' : None, 'settings' : None}

  def __init__(self):
    self.clearPlayersList()
    self.__controllers = {}
    self.__objects['settings'] = Models.Settings()
    self.__view = Views.MainView()
    self.setController('Menu')
    self.__view.show()

  def setController(self, controller, **kwargs):
    debug(controller)
    controller = controller + 'Controller'
    if controller not in self.__controllers:
      self.__controllers[controller] = globals()[controller](self.__view, self.__objects, self)
    self.__controller = self.__controllers[controller]
    self.__controller.run(**kwargs)

  def clearPlayersList(self):
    self.__objects['playerslist'] = {}

class MenuController(Controller):
  """ Controller for menu, starts other controllers """

  # View callbacks
  def newgame(self):
    debug()
    self._main.setController('NewGame')
  def joingame(self):
    debug()
    self._main.setController('JoinGame')
  def settings(self):
    debug()
    self._main.setController('Settings')
  def exit(self):
    debug()
    sys.exit(0)

class NewGameController(ClientExtension):
  """ Controller for prepare and provide UI of server class """

  def run(self):
    debug(self._objects)
    self._view.setView('NewGame', self)
    port = self._objects['settings'].get('Connection', 'port')

    if port is None:
      port = 0 # Automatic port setup
      self._objects['settings'].set('Connection', 'port', port)

    if not self._objects['server']:
      self._objects['server'] = Models.Server()

    self._objects['server'].setCallback(self)
    self._objects['server'].start(port, self._objects['playerslist'])

    if not self._objects['client']:
      self._objects['client'] = Models.Client()

    self._objects['client'].setCallback(self)
    self._objects['client'].start('127.0.0.1', port)

  # Views callbacks
  def startgame(self):
    self._objects['server'].deactiveAccept()
    self._objects['server'].send(CC.ALL, CC.STARTGAME)

  def backToMenu(self):
    self._objects['server'].stop()
    super(NewGameController, self).backToMenu()

  # Server callbacks
  def serverReceive(self, playerID, data):
    com, data = data

    if com == CC.NICK:
      try:
        nick = data['nick']
      except KeyError:
        debug('KeyError')
      else:
        if not self.__checkNick(nick):
          newNick = nick + '2'
          i = 3
          while not self.__checkNick(newNick):
            newNick = nick + str(i)
            i += 1
          self._objects['server'].send(playerID, CC.NEWNICK, {'nick':newNick})
          nick = newNick
        self._objects['playerslist'][playerID].nick = nick
        self._view.current.addPlayer(playerID, nick)

  def newClient(self, clientstock):
    debug(self._objects['playerslist'])
    playerID = len(self._objects['playerslist']) + 1
    self._objects['playerslist'][playerID] = Models.Player()
    self._objects['playerslist'][playerID].uri = clientstock[1] # not nessesary, but why no; so... :)
    self._objects['playerslist'][playerID].socket = clientstock[0]
    debug(self._objects['playerslist'])
    self._objects['server'].send(playerID, CC.NICK)
    return playerID

  def playerDisconected(self, playerID):
    debug('PL', playerID, self._objects['playerslist'])
    del self._objects['playerslist'][playerID]
    self._view.current.delPlayer(playerID)

  def connectError(self, error):
    self._view.showError('Chyba', error)
    self.backToMenu()
    #~ super(NewGameController, self).backToMenu()

  # Private methods
  def __checkNick(self, nick):
    for player in self._objects['playerslist'].values():
      if player.nick == nick:
        return False
    return True

class JoinGameController(ClientExtension):
  """ Controller for prepare client to connection to server """

  def run(self):
    if not hasattr(self, 'variables'):
      self.variables = {}

    super(JoinGameController, self).run()
    self._view.current.enableJoinButton()

  # Views callbacks
  def joingame(self):
    debug()
    remoteHost, remotePort = self.__parseIp(self.variables['remote'].get())
    if remoteHost is not None and remotePort is not None:
      if not self._objects['client']:
        self._objects['client'] = Models.Client()
      self._objects['client'].setCallback(self)
      self._objects['client'].start(remoteHost, remotePort)
    else:
      self._view.showWarning('Špatná IP adresa', 'Zadejte IP adresu v platném tvaru!\nnapř. 192.168.0.1:21561')

  def backToMenu(self):
    if self._objects['client']:
      self._objects['client'].stop()
    super(JoinGameController, self).backToMenu()

  # Client callbacks
  def refused(self):
    self._view.showError('Chyba', 'Server %s:%i odmítnul spojení'%self._objects['client'].serverInfo())
    self._view.current.enableJoinButton()

  def connected(self):
    debug()
    self._view.current.disableJoinButton()

  def clientStopped(self):
    self._view.showError('Chyba', 'Server %s:%i ukončil spojení'%self._objects['client'].serverInfo())
    super(JoinGameController, self).backToMenu()

  # Private methods
  def __parseIp(self, IP):
    try:
      host, port = IP.split(':')
    except ValueError:
      pass
    else:
      if host != '' and port != '':
        mask = re.compile('^(?:(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[0-9]{1,2}))\.){3}(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]{1,2})):\d{2,6}$')
        if re.match(mask, IP):
          return (host, int(port))
    return (None, None)

  # Testing methd for developers
  def localhost(self):
    self.variables['remote'].set('127.0.0.1:63500')
    self.joingame()

class SettingsController(Controller):
  """ Controller for settings view, work with configuration """

  def run(self):
    if not hasattr(self, 'variables'):
      self.variables = {}

    nick = self._objects['settings'].get('Player', 'nick')
    port = self._objects['settings'].get('Connection', 'port')

    cardsets = os.listdir(os.getcwd() + '/res/cards/')

    nick = nick if nick is not None else 'Player'
    port = port if port is not None else 63500
    cardsets = cardsets if cardsets is not None else 'scan'
    debug(cardsets)

    self.setView('Settings', nick=nick, port=port, cardsets=cardsets)

  # View callbacks
  def backToMenu(self):
    debug()
    nick = self.variables['nick'].get()
    port = self.variables['port'].get()
    cardsets = self.variables['cardset'].get()
    try:
      port = int(port)
      if port < 1024 or port > 65352:
        raise ConditionError
    except ValueError, ConditionError:
      self._view.showWarning('Neplatný port', 'Číslo portu musí být menší než 65362 a větší než 1024 (neprivilegovaný port)')
    else:
      if len(nick) < 3:
        self._view.showWarning('Neplatný nick', 'Nick, který jsi zadal je příliš krátký. Prodluž ho alespoň na 3 znaky.')
      elif re.match('^([\ ]*[\w\d])+$', nick) == None:
        self._view.showWarning('Neplatný nick', 'Nick, který jsi zadal obsahuje nepovolené znaky. Používej jenom písmena, čísla a mezery.')
      else:
        self._objects['settings'].set('Player', 'nick', nick)
        super(SettingsController, self).backToMenu()

class GameController(ClientExtension):
  """ Controller for game, checking rules and displaying """

  def run(self, nick):
    self.variables = {}
    self.__play = False
    self.__nick = nick
    self.__check = Models.Rules.Check()
    cardSet = self._objects['settings'].get('Game', 'cardset')
    if cardSet is None:
      self._objects['settings'].set('Game', 'cardset', 'scan')
    self.setView('Game', cardSet=cardSet)
    self.__GM = None
    self.__sevenActive = False
    self.__aceActive = False
    self.__currentCard = '  '
    if self._objects['server']:
      self.__GM = GMController(self._objects, self.__gameInterrupt, self._main)
    self._objects['client'].setCallback(self)
    self._objects['client'].send(CC.STATE, {'state':'ready'})

  # View callbacks
  def backToMenu(self):
    if self.__GM:
      self.__GM.stop()
      del self.__GM
    self._objects['client'].stop()
    super(GameController, self).backToMenu()

  def checkCard(self, card):
    debug(self.__play, self.__currentCard, card)
    if self.__play:
      suit, value = self.__currentCard[0], self.__currentCard[1:]

      if not self.__sevenActive and not self.__aceActive:
        if card[1] == 'S':
          self._view.current.showSportsman(card)
        elif self.__check.normal(self.__currentCard, card):
          self.__playCard(card)
        else:
          debug('wrong card value and suit')
          self.variables['infopanel'].set('Tuhle kartu nemůžeš použít')
      elif (  (card[1] == '7' and self.__sevenActive)  or
              (card[1] == 'A' and self.__aceActive)    ):
          self.__playCard(card)
      else:
        debug('wrong card only value')
        self.variables['infopanel'].set('Tuhle kartu nemůžeš použít')

  def getFreshCard(self, e):
    if self.__play:
      self._objects['client'].send(CC.FRESHCARD)
      debug(e)

  def sportsManSelect(self, card, suit):
    debug(suit)
    self._objects['client'].send(CC.PLAY, {'card':card, 'suit':suit})
    self.__play = False
    self._view.current.hideSportsman()
    self._view.current.removeCard(self.__nick, card)

  # Client callbacks
  def clientReceive(self, data):
    debug(data)
    com, data = data

    if com == CC.OUTOFCARDS:
      self._view.showInfo('Chyba', 'V balíčku došly karty')

    elif com == CC.ALLPLAYERS: # list of all players in game; sended after game start; draw all this oponents; send ready
      try:
        if isinstance(data['nickList'], list):
          self.__nickList = data['nickList']
          self.__nickList.remove(self.__nick)
        else:
          raise DataError
      except KeyError, DataError:
        debug('KeyError or DataEror')
      else:
        debug(self.__nickList)
        self._view.current.drawPlayers(self.__nickList)
        self._objects['client'].send(CC.STATE, {'state':'ready'})

    elif com == CC.CURRENTCARD: # set currentCard variable and change image in view; only at game init
      try:
        card = data['card']
      except KeyError:
        debug('KeyError')
      else:
        self._view.current.setCurrentCard(card)
        if self.__currentCard[1] != '7' and card[1] == '7':
          self.__sevenActive = True
        self.__currentCard = card

    elif com == CC.ALLCARDS: # all current players cards; draw this cards in view; save this cards into cards variable
      if isinstance(data, list):
        self._view.current.drawCurrentPlayer(self.__nick, data)
        self.__cards = data
        debug(data)

    elif com == CC.FRESHCARD:  # after request to server to new card from fresh cards, this callback handle the answer
      try:
        if isinstance(data['card'], list):
          cards = data['card']
        else:
          raise DataError
      except KeyError, DataError:
        debug('KeyError fresh card', data)
      else:
        debug('freshcard', cards)
        if len(cards) > 1:
          self.__sevenActive = False
        if len(cards) == 0:
          self.__aceActive = False
        for card in cards:
          debug('card', card)
          self.__cards.append(card)
          self._view.current.addCard(self.__nick, 1, card)

    elif com == CC.CURRENTPLAYER: # set play variable T/F on nick variable; print info to infomanel view variable
      try:
        if data['nick'] == self.__nick:
          debug('CURRENT CARD RECV', self.__currentCard)
          self.__play = True
          debug(self.__nick, 'HRAJU')
          self.variables['infopanel'].set('Jsi na řadě')
        else:
          self.__play = False
          debug(self.__nick, 'NEHRAJU :/')
          self.variables['infopanel'].set('Hraje hráč ' + str(data['nick']))
      except KeyError:
        pass

    elif com == CC.PLAYED: # after some opponent play some card; removing card from oponents view; setting current card
      try:
        player, currentCard = data['nick'], data['card']
      except KeyError:
        pass
      else:
        debug(player, self.__nick, self.__currentCard, currentCard)
        if self.__currentCard[1] == 'S':
          debug('hidding sportsman suit')
          self._view.current.hideSportsmanSuit()
        if player != self.__nick:
          self._view.current.removeCard(player)
        self._view.current.setCurrentCard(currentCard)

        if currentCard[1] == '7':
          self.__sevenActive = True
        if currentCard[1] == 'A':
          self.__aceActive = True

        self.__currentCard = currentCard
        if currentCard[1] == 'S':
          try:
            suit = data['suit']
            self._view.current.setSportsmanSuit(suit)
            self.__currentCard = suit + 'S' # view draw original card before and this set fake card for future checking
          except KeyError:
            debug('KeyError: player data suit')

    elif com == CC.NEWCARD:
      try:
        count = data['count']
        nick = data['nick']
      except KeyError:
        debug('KeyError new card')
      else:
        self._view.current.addCard(nick, count)
        if count > 1:
          self.__sevenActive = False
        if count == 0:
          self.__aceActive = False

    elif com == CC.WIN:
      try:
        nick = data['nick']
      except KeyError:
        debug('KeyError WIN nick')
      else:
        self._view.showInfo('Konec hry', 'Hráč {0} zvítězil!'.format(nick))
        self.backToMenu()

  def clientStopped(self):
    if not self._objects['server']:
      self._view.showError('Chyba', 'Server %s:%i ukončil spojení'%self._objects['client'].serverInfo())
      self.backToMenu()

  # Private methods
  def __gameInterrupt(self, player):
    self._view.showError('Chyba', 'Hráč ' + player + ' se předčasně odpojil ze hry')
    self.backToMenu()

  def __playCard(self, card):
    debug(self.__play, card)
    if self.__play:
      self._objects['client'].send(CC.PLAY, {'card':card})
      self.__play = False
      debug(self.__nick, card)
      self._view.current.removeCard(self.__nick, card)

class GMController(object):
  """ Class for controlling the game ~> Game Master """ # So c00l :))
  def __init__(self, objects, gameInterrupt, controller):
    self.__readyNodes = 0 # Clients and server sync
    self._objects = objects
    self.__gameInterrupt = gameInterrupt
    self._main = controller
    self._objects['server'].setCallback(self)
    self.__currentCard = ''
    self.__playersQueue = []
    self.__freshCards = []
    self.__usedCards = []
    self.__sevenCount = 0
    self.__sevenCard = False
    self.__aceCard = False
    self.__running = True

    for card in Models.Rules.Cards:
      for suit in Models.Rules.Suits:
        self.__freshCards.append(suit + card)

    random.shuffle(self.__freshCards)
    nickList = []

    for player in self._objects['playerslist'].keys():
      nickList.append(self._objects['playerslist'][player].nick)
      self.__freshCards, self._objects['playerslist'][player].cards = self.__freshCards[4:], self.__freshCards[:4]
      debug(self._objects['playerslist'][player], self._objects['playerslist'][player].cards)

    debug(nickList, type(nickList))
    self._objects['server'].send(CC.ALL, CC.ALLPLAYERS, {'nickList':nickList})
    debug(self.__freshCards)
    self.__readyNodes += 1

  # Server callbacks
  def playerDisconected(self, playerID):
    if self.__running:
      debug(self._objects['playerslist'], self._objects['playerslist'][playerID])
      self.__gameInterrupt(str(self._objects['playerslist'][playerID]))
      #~ del self._objects['playerslist'][playerID]

  def serverReceive(self, playerID, data):
    debug(playerID, data)
    com, data = data

    if com == CC.FRESHCARD:
      debug('new card request')
      if self.__sevenCard: # seven card action
        debug('seven card', self.__sevenCount)
        cards = []

        for i in range(self.__sevenCount):
          card = self.__getNewCard()
          if card is not False:
            cards.append(card)
          else:
            self._objects['server'].send(playerID, CC.OUTOFCARDS)
            self.__nextPlayer()
            self.__sevenCount = 0
            self.__sevenCard = False
            return

        self._objects['server'].send(playerID, CC.FRESHCARD, {'card':cards})
        self._objects['playerslist'][playerID].cards = self._objects['playerslist'][playerID].cards + cards
        count = self.__sevenCount
        self.__sevenCount = 0
        self.__sevenCard = False

      elif self.__aceCard:
        self._objects['server'].send(playerID, CC.FRESHCARD, {'card':[]})
        count = 0
        self.__aceCard = False

      else:
        card = self.__getNewCard()
        if card is not False:
          self._objects['server'].send(playerID, CC.FRESHCARD, {'card':[card]})
          self._objects['playerslist'][playerID].cards.append(card)
          count = 1
        else:
          self._objects['server'].send(playerID, CC.OUTOFCARDS)
          self.__nextPlayer()
          count = -1

      debug('AFTER FRESHCARD', self._objects['playerslist'][playerID].cards)
      if count != -1:
        for player in self.__playersQueue:
          if player != self.__currentPlayer:
            self._objects['server'].send(player, CC.NEWCARD, {'nick':self._objects['playerslist'][playerID].nick, 'count':count})

      self.__nextPlayer()

    elif com == CC.STATE:
      try:
        state = data['state']
      except KeyError:
        debug('KeyError')
      else:
        if data['state'] == 'ready':
          self.__readyNodes += 1
          if self.__readyNodes == (len(self._objects['playerslist']) + 1):
            self.__startGame() # <<<<<<<<<<<<<< nazev???

    elif com == CC.PLAY:
      debug('PlayerID:' + str(playerID) + ' \currentPlayer:' + str(self.__currentPlayer))
      try:
        # we will trust the client, check on server comes maybe in next update.. ;)
        card = data['card']
      except KeyError:
        debug('KeyError PLAY', data, self.__usedCards)
      else:

        if ( playerID == self.__currentPlayer
             and card in self._objects['playerslist'][playerID].cards ):
          self.__usedCards.append(self.__currentCard)
          self.__currentCard = card
          debug('GM PLAY CURRENT CARD', self.__currentCard)
          debug(self._objects['playerslist'][playerID].cards, data['card'])
          self._objects['playerslist'][playerID].cards.remove(data['card'])
          debug(self.__usedCards)
          if len(self._objects['playerslist'][playerID].cards) == 0:
            self.__endGame(self._objects['playerslist'][playerID].nick)
            return 0

          if self.__currentCard[1] == '7':
            self.__sevenCount += 2
            self.__sevenCard = True
            debug('seven card', self.__sevenCount)

          if self.__currentCard[1] == 'A':
            self.__aceCard = True

          answer = {'nick':str(self._objects['playerslist'][playerID]), 'card':data['card']}

          try:
            answer['suit'] = data['suit']
          except KeyError:
            debug('KeyError PLAY data suit')
          self._objects['server'].send(CC.ALL, CC.PLAYED, answer)
          self.__nextPlayer()

  def stop(self):
    self.__running = False
    self._objects['server'].stop()
    self._main.clearPlayersList()

  def socketError(self, e):
    #~ self._view.showError('Chyba', 'Při pokusu o komunikaci se vyskytla kritická chyba. Prosím, restarujte aplikaci!\n' + str(e))
    sys.exit(1)

  # Private methods
  def __startGame(self):
    for player in self._objects['playerslist']:
      self._objects['server'].send(player, CC.ALLCARDS, self._objects['playerslist'][player].cards)
      self.__playersQueue.append(player)
    self.__currentPlayer = self.__playersQueue[-1]
    self.__currentCard = self.__getNewCard()
    self._objects['server'].send(CC.ALL, CC.CURRENTCARD, {'card':self.__currentCard})
    self.__nextPlayer()
    debug()

    if self.__currentCard[1] == '7':
      self.__sevenCard = True
      self.__sevenCount = 2

    if self.__currentCard[1] == 'A':
      self.__aceCard = True

  def __endGame(self, nick):
    self._objects['server'].send(CC.ALL, CC.WIN, {'nick':nick})
    self.stop()

  def __getNewCard(self):
    debug(self.__freshCards, self.__usedCards)
    debug(len(self.__freshCards), len(self.__usedCards))

    if len(self.__freshCards) == 0:
      if len(self.__usedCards) == 0:
        return False
      random.shuffle(self.__usedCards)
      self.__freshCards = self.__usedCards
      self.__usedCards = []

    debug(len(self.__freshCards), len(self.__usedCards))
    card = self.__freshCards[0]
    del self.__freshCards[0]
    debug(len(self.__freshCards), len(self.__usedCards), card)
    return card

  def __nextPlayer(self):
    debug(self.__playersQueue, self.__currentPlayer)
    nextPlayer = self.__playersQueue.index(self.__currentPlayer) + 1

    if nextPlayer + 1 > len(self.__playersQueue):
      nextPlayer = 0
    nextPlayer = self.__playersQueue[nextPlayer]

    self.__currentPlayer = nextPlayer
    debug(self.__playersQueue, nextPlayer)
    self._objects['server'].send(CC.ALL, CC.CURRENTPLAYER, {'nick':str(self._objects['playerslist'][nextPlayer])})

if __name__ == '__main__':
  import Run
  Run.main()

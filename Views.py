#!/usr/bin/env python
# -*- coding: utf-8 -*-

# DEV: May be cool in future :)
# self._root.winfo_children()

# Imports
from Tkinter import *
from Debug   import *
import tkMessageBox
import os

def resources(file):
  path = os.getcwd() + '/res/' + file
  debug(path)
  return path

class View(object):
  """ Templare class for partial views classes """
  def __init__(self, callback, root):
    debug()
    self.name = self.__class__.__name__[:-4]
    self._callback = callback
    self._root = Frame(root)
  def __repr__(self):
    return self.name
  def show(self):
    self._root.pack()
  def hide(self):
    self._root.pack_forget()
class PlayersListExtension(View):
  def addPlayer(self, playerID, nick):
    self._playersIndex.append(playerID)
    self._playersListbox.insert(END, nick)
    if len(self._playersIndex) == 2:
      self._launchButton['state'] = 'normal'
    debug(self._playersIndex, self._playersListbox.get(0, END))
  def delPlayer(self, playerID):
    try:
      index = self._playersIndex.index(playerID)
      del self._playersIndex[index]
      self._playersListbox.delete(index)
      if len(self._playersIndex) == 1:
        self._launchButton['state'] = 'disabled'
      debug(self._playersIndex, self._playersListbox.get(0, END))
    except ValueError:
      pass
  def _delAllPlayers(self):
    self._playersListbox.delete(0, END)
    self._playersIndex = []

class MainView(object):
  """ Tk handler and view and callback manager """
  def __init__(self):
    self.__root = Tk()
    width = 285
    height = 360
    self.__root.geometry(("%ix%i")%(width, height))
    #~ self.__root.resizable(width=False, height=False)
    self.__root.title('Prší')
    #~ self.__root.iconbitmap(resources('icon.ico')) # todo program icon :D
    self.__views = {}
    self.current = None

  def setView(self, view, callback, **kwargs):
    debug(view)
    #~ self.__callback = callback
    if self.current:
      self.current.hide()
    if str(view) not in self.__views: # Store objects or not? THAT'S THE QUESTION!!
                                      #  Now it seem that it isn't, because of need of some
                                      #  components in fresh state after views next recall
      self.__views[view] = globals()[view + 'View'](callback, self.__root)
      self.__views[view].build(**kwargs)
    self.current = self.__views[view]
    self.current.show()

  def show(self):
    debug()
    self.__root.mainloop()

  def showWarning(self, title, message):
    tkMessageBox.showwarning(title, message)
  def showError(self, title, message):
    tkMessageBox.showerror(title, message)
  def showInfo(self, title, message):
    tkMessageBox.showinfo(title, message)

class MenuView(View):
  def build(self):
    pimg = PhotoImage(file=resources('menu_background.gif'))
    background = Label(self._root, image=pimg, bg='black', width=285, height=360)
    background.image = pimg

    pbtn = {'bd':0, 'bg':'#FFFFFF'}
    pplc = {'x':20, 'width':115, 'height':25}

    Button(background, text='Nová hra', command=self._callback.newgame, **pbtn).place(y=60, **pplc)
    Button(background, text='Připojit se ke hře', command=self._callback.joingame, **pbtn).place(y=90, **pplc)
    Button(background, text='Nastavení', command=self._callback.settings, **pbtn).place(y=120, **pplc)
    Button(background, text='Ukončit hru', command=self._callback.exit, **pbtn).place(y=150, **pplc)

    background.pack()
class NewGameView(PlayersListExtension):
  def build(self):
    self._playersIndex = []
    self._playersListbox = Listbox(self._root)
    self._playersListbox.pack()
    self._launchButton = Button(self._root, text='Zahájit hru', command=self._callback.startgame, state=DISABLED)
    self._launchButton.pack()
    Button(self._root, text='Odpojit se', command=self._callback.backToMenu).pack()
  def show(self):
    self._launchButton['state'] = 'disabled'
    super(NewGameView, self).show()
  def hide(self):
    self._delAllPlayers()
    super(NewGameView, self).hide()
class JoinGameView(PlayersListExtension):
  def build(self):
    #~ self.__serverLabel = Label(self._root, text='Server')
    #~ self.__serverLabel.pack()
    Button(self._root, text='localhost test', command=self._callback.localhost).pack()
    self._callback.variables['remote'] = StringVar()
    Entry(self._root, textvariable=self._callback.variables['remote']).pack()
    self.__joinButton = Button(self._root, text='Připojit se', command=self._callback.joingame)
    self.__joinButton.pack()
    self.__menuButton = Button(self._root, text='Odpojit se', command=self._callback.backToMenu)
    self.__menuButton.pack()
  def enableJoinButton(self):
    self.__joinButton['state'] = 'normal'
    self.__menuButton['text'] = 'Zpět do menu'
  def disableJoinButton(self):
    self.__joinButton['state'] = 'disabled'
    self.__menuButton['text'] = 'Odpojit se'
class SettingsView(View):
  def build(self, nick, port, cardsets):
    Label(self._root, text='Přezdívka').grid(row=0, column=0)
    self._callback.variables['nick'] = StringVar()
    self._callback.variables['nick'].set(nick)
    Entry(self._root, textvariable=self._callback.variables['nick']).grid(row=0, column=1)
    Label(self._root, text='Herní port').grid(row=1, column=0)
    self._callback.variables['port'] = StringVar()
    self._callback.variables['port'].set(port)
    Entry(self._root, textvariable=self._callback.variables['port']).grid(row=1, column=1)
    Label(self._root, text='Vzhled karet').grid(row=2, column=0)
    self._callback.variables['cardset'] = StringVar()
    self._callback.variables['cardset'].set(cardsets[0])
    #~ Entry(self._root, textvariable=self._callback.variables['cardset']).grid(row=2, column=1)
    om = apply(OptionMenu, (self._root, self._callback.variables['cardset']) + tuple(cardsets))
    om.grid(row=2, column=1)
    Button(self._root, text='Zpět do menu', command=self._callback.backToMenu).grid(row=3, column=0, columnspan=2)

class GameView(object):
  def __init__(self, callback, root):
    self.name = self.__class__.__name__[:-4]
    self._callback = callback
    self._mainWindow = root
    self.__originalColor = self._mainWindow.cget('bg')
    self.__gameColor = '#00AA00'
    self._root = Frame(root, bg=self.__gameColor)

    self.__cardSet = 'scan'
    self.__currentCard = {'label':None, 'image':PhotoImage(file=self.__cardImagePath('unkown')), 'name':'unkown'}
    self.__frames = {}
  def __repr__(self):
    return self.name
  def show(self):
    self._root.pack(expand=1)
    self._mainWindow.geometry('505x460')
    self._mainWindow.configure(background=self.__gameColor)
    self._callback.variables['infopanel'] = StringVar()
    self._callback.variables['infopanel'].set('Hra se načítá (mělo by to trvat méně než 3 vteřiny)')
    self.__infopanel = Label(self._root, textvariable=self._callback.variables['infopanel'], bg=self.__gameColor)
    self.__infopanel.grid(row=5, column=0, columnspan=2)
  def hide(self):
    for player in self.__playersList.keys():
      self.__playersList[player]['cardsframe'].grid_forget()
    self._mainWindow.geometry('285x360')
    self._mainWindow.configure(background=self.__originalColor)
    self.__infopanel.grid_forget()
    self._root.pack_forget()
  def build(self, cardSet='scan'):
    Button(self._root, text='Ukončit', command=self._callback.backToMenu).grid(row=5, column=2)
    self.__cardSet = cardSet
    debug(self.__cardSet)
  def drawPlayers(self, playersList):
    self.__playersList = {}
    for player in playersList:
      self.__playersList[player] = {'cards':4}
    countOfPlayers = len(self.__playersList)
    debug(countOfPlayers, self.__playersList)
    self.setCurrentCard('unkown')
    freshCardsImage = PhotoImage(file=self.__cardImagePath('fresh'))
    freshCardsLabel = Label(self._root, image=freshCardsImage, bg=self.__gameColor, cursor='hand2')
    freshCardsLabel.image = freshCardsImage
    freshCardsLabel.grid(row=2, column=0)
    freshCardsLabel.bind('<Button-1>', self._callback.getFreshCard)
    self.__preparePlayersCard(countOfPlayers - 1)
  def setCurrentCard(self, filename):
    self.__currentCard['image'] = PhotoImage(file=self.__cardImagePath(filename))
    self.__currentCard['label'] = Label(self._root, image=self.__currentCard['image'], bg=self.__gameColor)
    self.__currentCard['label'].image = self.__currentCard['image']
    self.__currentCard['label'].grid(row=2, column=1, sticky=W+E+N+S)
    debug()
  def drawCurrentPlayer(self, player, cards):
    self.__playersList[player] = {'cards':cards}
    nick = Label(self._root, text=player, bg=self.__gameColor)
    nick.grid(row=3, column=2, sticky=S+E)
    self.drawCurrentPlayerCards(player, cards)
  def drawCurrentPlayerCards(self, player, cards):
    try:
      self.__playersList[player]['cardsframe'].grid_forget()
      self.__playersList[player]['cardsframe'].destroy()
    except KeyError:
      debug('[][][][][] KeyError player cardsframe [] !!')
    else:
      debug('[][][][][ Key OK player cardsframe [] !!')
    self.__playersList[player]['cardsframe'] = Frame(self._root, bg=self.__gameColor)
    for card in cards:
      photoimage = PhotoImage(file=self.__cardImagePath(card))
      label = Label(self.__playersList[player]['cardsframe'], image=photoimage, bg=self.__gameColor, cursor='hand2')
      label.image = photoimage
      label.pack(side=LEFT)
      label.bind('<Button-1>', lambda e, card=card:self._callback.checkCard(card))
    self.__playersList[player]['cardsframe'].grid(row=4, column=0, columnspan=3)
  def showSportsman(self, card):
    debug()
    def drawSuit(suit):
      photoimage = PhotoImage(file=self.__cardImagePath(suit))
      label = Label(self.__sportsman, image=photoimage, bg=self.__gameColor, cursor='hand2')
      label.image = photoimage
      label.pack(side=LEFT)
      label.bind('<Button-1>', lambda e, suit=suit:self._callback.sportsManSelect(card, suit))
    if not hasattr(self, '__sportsman'):
      self.__sportsman = Frame(self._root, bg=self.__gameColor)
      for suit in ['H', 'A', 'L', 'S']:
        drawSuit(suit)
    self.__sportsman.grid(row=4, column=0, columnspan=3)
  def hideSportsman(self):
    try:
      self.__sportsman.grid_forget()
    except AttributeError:
      pass
  def setSportsmanSuit(self, suit):
    debug(suit)
    photoimage = PhotoImage(file=self.__cardImagePath(suit))
    self.__sportsmansuit = Label(self._root, image=photoimage, bg=self.__gameColor)
    self.__sportsmansuit.image = photoimage
    self.__sportsmansuit.grid(row=2, column=2)
  def hideSportsmanSuit(self):
    #~ if hasattr(self, '__sportsmansuit'):
    try:
      self.__sportsmansuit.grid_forget()

    except AttributeError:
      debug('self has no attr __sportsmansuit')
    #~ debug(hasattr(self, '__sportsmansuit'))
    #~ debug(hasattr(self, 'sportsmansuit'))
  def removeCard(self, player, card=None):
    if card is None:
      debug(card, self.__playersList[player]['cards'])
      self.__playersList[player]['cards'] -= 1
      self.__drawPlayerCards(player)
    else:
      debug(card)
      self.__playersList[player]['cards'].remove(card)
      self.drawCurrentPlayerCards(player, self.__playersList[player]['cards'])
  def addCard(self, player, count, card=None):
    debug(player, count, card)
    if card is None:
      self.__playersList[player]['cards'] += count
      self.__drawPlayerCards(player)
    else:
      self.drawCurrentPlayerCards(player, self.__playersList[player]['cards'])

  def __preparePlayersCard(self, count):
    player = self.__playersList.keys()[count]
    self.__playersList[player]['rowindex'] = count * 2
    self.__drawPlayerCards(player)
    nick = Label(self._root, text=player, bg=self.__gameColor)
    nick.grid(row=(count * 2 + 1), column=0, sticky=W+N)

  #~ def __twoPlayersView(self):
    #~ player1 = self.__playersList.keys()[0]
    #~ self.__playersList[player1]['rowindex'] = 0
    #~ self.__drawPlayerCards(player1)
    #~ nick1 = Label(self._root, text=player1, bg=self.__gameColor)
    #~ nick1.grid(row=1, column=0, sticky=W+N)
  #~ def __threePlayersView(self):
#~
    #~ player2 = self.__playersList.keys()[1]
    #~ self.__playersList[player2]['rowindex'] = 2
    #~ self.__drawPlayerCards(player2)
    #~ nick2 = Label(self._root, text=player1, bg=self.__gameColor)
    #~ nick2.grid(row=3, column=0, sticky=W+N)
#~
  #~ def __fourPlayersView(self):
    #~ player1 = self.__playersList.keys()[0]
    #~ player2 = self.__playersList.keys()[1]
    #~ player3 = self.__playersList.keys()[2]
#~
    #~ self.__drawPlayerCards(player1)
#~
    #~ self.__frames['centerframe'] = Frame(self._root, bg=self.__gameColor)
    #~ nick1 = Label(self.__frames['centerframe'], text=player1, bg=self.__gameColor)
    #~ nick1.grid(row=0, column=0, sticky=W+N)
    #~ nick2 = Label(self.__frames['centerframe'], text=player1, bg=self.__gameColor)
    #~ nick2.grid(row=0, column=0, sticky=W+N)
    #~ self.setCurrentCard('unkown')
    #~ nick3 = Label(self.__frames['centerframe'], text=player2, bg=self.__gameColor)
    #~ nick3.grid(row=2, column=2, sticky=S+E)
    #~ nick4 = Label(self.__frames['centerframe'], text=player2, bg=self.__gameColor)
    #~ nick4.grid(row=2, column=2, sticky=S+E)
    #~ self.__frames['centerframe'].pack()
#~
    #~ self.__drawPlayerCards(player2)
  def __cardImagePath(self, image):
    return 'res/cards/' + self.__cardSet + '/' + image + '.gif'
    #~ return resources('cards/' + self.__cardSet + '/' + image + '.gif')
    pass
  def __drawPlayerCards(self, player):
    rowIndex = self.__playersList[player]['rowindex']
    try:
      self.__playersList[player]['cardsframe'].grid_forget()
      self.__playersList[player]['cardsframe'].destroy()
    except KeyError:
      debug('KeyError')
    self.__playersList[player]['cardsframe'] = Frame(self._root, bg=self.__gameColor)
    for i in range(self.__playersList[player]['cards']):
      photoimage = PhotoImage(file=self.__cardImagePath('unkown'))
      label = Label(self.__playersList[player]['cardsframe'], image=photoimage, bg=self.__gameColor)
      label.image = photoimage
      label.pack(side=LEFT)
    self.__playersList[player]['cardsframe'].grid(row=rowIndex, column=0, columnspan=3)
  #~ def __drawCurrentPlayerCard(self, cards):
if __name__ == '__main__':
  import Run
  Run.main()

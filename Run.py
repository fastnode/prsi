#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Run.py
#
#  Copyright 2014 Ladislav Drahokoupil, Jakub Kutiš, Jan Skutil
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

##::~~-VARIABLES-~~::##   ##::~~- MODULES -~~::##   ##::~~-  FILES  -~~::##
#                     #   #                     #   #                     #
# var   -> public     #   # tkMessageBox        #   # Controllers.py      #
# _var  -> protected  #   # ConfigParser        #   # Debug.py            #
# __var -> private    #   # threading           #   # Models.py           #
#                     #   # Tkinter             #   # README.md           #
##::~~-ªºªºªºªºª-~~::##   # inspect             #   # Run.py              #
                          # random              #   # Views.py            #
                          # thread              #   # player.cfg          #
                          # errno               #   #                     #
                          # json                #   ##::~~- ºªºªºªº -~~::##
                          # sys                 #
                          # os                  #
                          # re                  #
                          #                     #
                          ##::~~- ºªºªºªº -~~::##


# Imports
from Controllers import MainController

def main():
  MainController()
  return 0

if __name__ == '__main__':
  main()
